## Description
Link-Metadata used to get metadata from internet URLs (htmls, images, media..).
It also returns security policy information (Cross-Origin Resource Sharing, Content Security Policy) and checks them with Google Safe Browsing (Web Risk).

## Installation
```console
$ npm install @binarycortex/link-metadata
```
## Usage
Link-Metadata is TypeScript-friendly and comes with both CommonJS and ESM module support.

```js
const lmd = require('@binarycortex/link-metadata');
```
or
```js
import lmd from '@binarycortex/link-metadata';
```

First, log in and create a new access key. If you already have one you don't need to login, just use the key.

```js
const username = 'user';
const password = 'password';

const response = await lmd.login(username, password);

if (response.error) {
    console.error(response.error);
    return;
}

const key = response.accessKey;
```

Getting a metadata for the given URL:

```js
const metadata = await lmd.getMetadata(url, key);
```

## API

### login(username, password)

Used to get first instance of access key. Returns LoginResponse:

```js
interface AccessKey {
    accessId: string;
    accessSecret: string;
}

interface LoginResponse {
    username?: string;
    accessKey?: AccessKey;
    error?: string;
}
```

If an error occurs, it is returned in the corresponding field.

### createKey(key)

Issue a new key (using current). All keys are the same in terms of functionality, there is no 'primary' key. You may create as much keys as you want. Returns LoginResponse.

### getKeys(key)

List all available keys. Returns KeysResponse:

```js
interface KeysResponse {
    username?: string;
    accessKeys?: AccessKey[];
    error?: string;
}
```

### revokeKey(key)

Revokes key (the one that passed in argument). Returns a string.
