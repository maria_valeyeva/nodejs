// Security policy information
export interface Security {
    cors: {};
    csp: {};
    "x-frame-options": string | null;
}

// Google's Web Risk result
export interface GoogleSafeBrowsing {
    isUrlSafe: boolean;
    result: {
        threat: {
            threatTypes: string[];
            expireTime: string;
        }
    } | {}
}

// All image/video/html/etc extend from file.
export interface File {
    url: string; // The final url after redirection for the target resource
    size: number; // The size in bytes of the resource
    security: Security;
}

// Extra rss information available if target file is an rss xml file.
export interface XML_RSS {
    version: string | null; // RSS version
    language: string | null; // The language for the rss feed
    title: string | null; // Title of the rss if available
    description: string | null; // Description of the rss if available.
}

// IF the target file is xml. Parse a couple values from the xml
export interface XML extends File {
    version: string; // XML version.
    rss: XML_RSS | null; // If the file appears to be an rss, get some data about the rss
}

// Media files are image/video/audio
export interface Media extends File {
    format: string; // The file format (jpg/mp4/mp3/etc)
}

// Represents an image file
export interface Image extends Media {
    height: number | null; // Height in pixels
    width: number | null; // Width in pixels
}

// Video file
export interface Video extends Media {
    height: number | null; // Heigh of video in pixels
    width: number | null; // Width of video in pixels
    duration: number | null; // The duration of the video. Probably in milliseconds.
}

// Audio file
export interface Audio extends Media {
    duration: number | null; // Duration of the file in milliseconds
}

// And HTML file. AKA a website
export interface HTML extends File {
    title: string | null; // Meta title
    description: string | null; // Meta description
    author: string | null; // If present the author of the page
    publisher: string | null; // If present the publisher for the page
    language: string | null; // The language of the page, as determined by meta not a text analysis
    languageAlternates: string[]; // Alternative translations if they exist
    websiteName: string | null; // Human readable name for the website. Yahoo for yahoo.com for example.
    image: Image | null; // Image to be displayed in previews
    video: Video | null; // Any video to display in previews
    audio: Audio | null; // Any audio to display in previews
    favicon: Image | null; // The favicon for the website
    // ldJson: Record<string, unknown>; // The raw ld+json in the site. We pull some information from this
    // But the client may want more of the details available in the ld+json.
}

// The response format from the api
export interface Response {
    url: string; // The original request url
    status: number | null; // Request status code. Null in cases where url is invlaid and the like. Never make the network request

    type: string | null; // The type of the content. This should be a static list (enum). This may be base on
    // the file such as video. Or more specific like article/book/etc. Full list to be determined.
    contentType: string | null; // HTML mime time from the http request
    contentEncoding: string | null; // HTML content encoding from the http request

    audio: Audio | null; // If the file is an audio file, audio details will be here.
    file: File | null; // If the file is not any of the other supported types, what details we have will be here.
    html: HTML | null; // If the file is an html file, html details will be here.
    image: Image | null; // If the file is an image file, image details will be here.
    video: Video | null; // If the file is an video file, video details will be here.
    xml: XML | null; // If the file is an xml file, xml details will be here.

    // errors
    error: number | null; // Number indicates there was an error and is the error code
    reason: string | null; // Reason string for error
    warnings: string[] | null; // A list of warnings/errors while processing. See [Warnings](#warnings) section for some examples

    // Web Risk
    googleSafeBrowsing: GoogleSafeBrowsing | null;
}