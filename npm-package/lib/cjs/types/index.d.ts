import { Response } from './Interfaces';
export interface AccessKey {
    accessId: string;
    accessSecret: string;
}
export interface LoginResponse {
    username?: string;
    accessKey?: AccessKey;
    error?: string;
}
export interface KeysResponse {
    username?: string;
    accessKeys?: AccessKey[];
    error?: string;
}
export declare function login(username: string, password: string): Promise<LoginResponse>;
export declare function createKey(key: AccessKey): Promise<LoginResponse>;
export declare function getKeys(key: AccessKey): Promise<KeysResponse>;
export declare function revokeKey(key: AccessKey): Promise<string>;
export declare function getMetadata(url: string, key: AccessKey): Promise<Response>;
export default getMetadata;
//# sourceMappingURL=index.d.ts.map