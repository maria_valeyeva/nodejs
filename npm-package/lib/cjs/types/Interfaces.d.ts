export interface Security {
    cors: {};
    csp: {};
    "x-frame-options": string | null;
}
export interface GoogleSafeBrowsing {
    isUrlSafe: boolean;
    result: {
        threat: {
            threatTypes: string[];
            expireTime: string;
        };
    } | {};
}
export interface File {
    url: string;
    size: number;
    security: Security;
}
export interface XML_RSS {
    version: string | null;
    language: string | null;
    title: string | null;
    description: string | null;
}
export interface XML extends File {
    version: string;
    rss: XML_RSS | null;
}
export interface Media extends File {
    format: string;
}
export interface Image extends Media {
    height: number | null;
    width: number | null;
}
export interface Video extends Media {
    height: number | null;
    width: number | null;
    duration: number | null;
}
export interface Audio extends Media {
    duration: number | null;
}
export interface HTML extends File {
    title: string | null;
    description: string | null;
    author: string | null;
    publisher: string | null;
    language: string | null;
    languageAlternates: string[];
    websiteName: string | null;
    image: Image | null;
    video: Video | null;
    audio: Audio | null;
    favicon: Image | null;
}
export interface Response {
    url: string;
    status: number | null;
    type: string | null;
    contentType: string | null;
    contentEncoding: string | null;
    audio: Audio | null;
    file: File | null;
    html: HTML | null;
    image: Image | null;
    video: Video | null;
    xml: XML | null;
    error: number | null;
    reason: string | null;
    warnings: string[] | null;
    googleSafeBrowsing: GoogleSafeBrowsing | null;
}
//# sourceMappingURL=Interfaces.d.ts.map