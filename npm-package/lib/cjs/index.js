"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMetadata = exports.revokeKey = exports.getKeys = exports.createKey = exports.login = void 0;
const buffer_1 = require("buffer");
const node_fetch_1 = __importDefault(require("node-fetch"));
const LMD_URL = (_a = process.env.LMD_URL) !== null && _a !== void 0 ? _a : 'http://localhost:3000';
function login(username, password) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, node_fetch_1.default)(`${LMD_URL}/login`, {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: { 'Content-Type': 'application/json' },
        });
        return response.ok ?
            yield response.json() :
            { error: yield response.text() };
    });
}
exports.login = login;
function createKey(key) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, node_fetch_1.default)(`${LMD_URL}/createKey`, {
            method: 'POST',
            headers: { 'Authorization': 'Basic ' + keyBase64(key) },
        });
        return response.ok ?
            yield response.json() :
            { error: yield response.text() };
    });
}
exports.createKey = createKey;
function getKeys(key) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, node_fetch_1.default)(`${LMD_URL}/getKeys`, {
            method: 'POST',
            headers: { 'Authorization': 'Basic ' + keyBase64(key) },
        });
        return response.ok ?
            yield response.json() :
            { error: yield response.text() };
    });
}
exports.getKeys = getKeys;
function revokeKey(key) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, node_fetch_1.default)(`${LMD_URL}/revokeKey`, {
            method: 'POST',
            headers: { 'Authorization': 'Basic ' + keyBase64(key) },
        });
        return yield response.text();
    });
}
exports.revokeKey = revokeKey;
function getMetadata(url, key) {
    return __awaiter(this, void 0, void 0, function* () {
        const objURL = new URL(url);
        url = objURL.origin + objURL.pathname + encodeURIComponent(objURL.search);
        const response = yield (0, node_fetch_1.default)(`${LMD_URL}/SpinItOn?&url=${url}`, {
            headers: { 'Authorization': 'Basic ' + keyBase64(key) },
        });
        return yield response.json();
    });
}
exports.getMetadata = getMetadata;
// auxiliary functions
// ...
function keyBase64(key) {
    const buff = buffer_1.Buffer.from(`${key.accessId}:${key.accessSecret}`, 'utf-8');
    return buff.toString('base64');
}
exports.default = getMetadata;
