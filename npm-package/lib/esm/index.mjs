import { Buffer } from 'buffer';
import fetch from 'node-fetch';
const LMD_URL = process.env.LMD_URL ?? 'http://localhost:3000';
export async function login(username, password) {
    const response = await fetch(`${LMD_URL}/login`, {
        method: 'POST',
        body: JSON.stringify({ username, password }),
        headers: { 'Content-Type': 'application/json' },
    });
    return response.ok ?
        await response.json() :
        { error: await response.text() };
}
export async function createKey(key) {
    const response = await fetch(`${LMD_URL}/createKey`, {
        method: 'POST',
        headers: { 'Authorization': 'Basic ' + keyBase64(key) },
    });
    return response.ok ?
        await response.json() :
        { error: await response.text() };
}
export async function getKeys(key) {
    const response = await fetch(`${LMD_URL}/getKeys`, {
        method: 'POST',
        headers: { 'Authorization': 'Basic ' + keyBase64(key) },
    });
    return response.ok ?
        await response.json() :
        { error: await response.text() };
}
export async function revokeKey(key) {
    const response = await fetch(`${LMD_URL}/revokeKey`, {
        method: 'POST',
        headers: { 'Authorization': 'Basic ' + keyBase64(key) },
    });
    return await response.text();
}
export async function getMetadata(url, key) {
    const objURL = new URL(url);
    url = objURL.origin + objURL.pathname + encodeURIComponent(objURL.search);
    const response = await fetch(`${LMD_URL}/SpinItOn?&url=${url}`, {
        headers: { 'Authorization': 'Basic ' + keyBase64(key) },
    });
    return await response.json();
}
// auxiliary functions
// ...
function keyBase64(key) {
    const buff = Buffer.from(`${key.accessId}:${key.accessSecret}`, 'utf-8');
    return buff.toString('base64');
}
export default getMetadata;
